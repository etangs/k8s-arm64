FROM alpine:3.14
RUN wget -q https://dl.k8s.io/release/v1.21.0/bin/linux/arm64/kubectl && \
    mv kubectl /usr/bin/ && \
    chmod +x /usr/bin/kubectl
RUN wget -q https://get.helm.sh/helm-v3.6.3-linux-arm64.tar.gz && \
    tar zxf helm*.tar.gz && \
    mv linux-arm64/helm /usr/bin/ && \ 
    chmod +x /usr/bin/helm
RUN mkdir /root/.kube
RUN rm -rf helm*.tar.gz linux-arm64 kubectl
